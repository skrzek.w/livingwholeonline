<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

get_header(); 

$coursector_topbar = coursector_get_topbar();

/**
*	Get current page id
**/

$current_page_id = $post->ID;

//Include custom header feature
get_template_part("/templates/template-post-header");
?>
    
    <div class="inner">

    	<!-- Begin main content -->
    	<div class="inner_wrapper">

    		<div class="sidebar_content full_width blog_f">
					
<?php
if (have_posts()) : while (have_posts()) : the_post();
?>
						
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
		<?php
            $event_product = get_field('event_product');
		    $current_user = wp_get_current_user();
            
            if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $event_product[0] ) ) {
                the_content();
            }else {
                _e('You need to buy membership and event to see that content');
            };
		?>
    </div>

</div>
<!-- End each blog post -->

<?php
if(comments_open($post->ID) OR coursector_post_has('pings', $post->ID)) 
{
?>
<div class="fullwidth_comment_wrapper sidebar themeborder"><?php comments_template( '', true ); ?></div>
<?php
}
?>

<?php
//Get post navigation
get_template_part("/templates/template-post-navigation");
?>

<?php endwhile; endif; ?>
						
    	</div>
    
    </div>
    <!-- End main content -->
</div>

<br class="clear"/>
</div>
<?php get_footer(); ?>