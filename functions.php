<?php
// WPKRAKEN CHANGES
class WC_Product_Event extends WC_Product_Simple {

	// return product type 
	public function get_type() {
		return 'event';
	}

	// show price to memberships only
	public function get_price($context = 'view') {
		if(is_user_logged_in()) {
			$price = $this->get_meta('_event_price', true);
			if(is_numeric($price)){
				return $price;
			}
		}
		return $this->get_prop('price', $context);
	}

}

// dropdown 
function add_event_to_dropdown($types) {
	$types['event'] = __('Event', 'coursector');
	return $types;
}
add_filter('product_type_selector', 'add_event_to_dropdown');

// product as a taxonomy
function event_taxonomy() {
	if(!get_term_by('slug', 'event', 'product_type')){
		wp_insert_term('event', 'product_type');
	}
}
register_activation_hook(__FILE__, 'event_taxonomy');

add_action('woocommerce_product_options_general_product_data', function(){
	echo '<div class="options_group show if_event"></div>';
});

// event js
function event_product_js() {
	global $post, $product_object;

	if(!$post) return;

	if('product' != $post->post_type){
		return;
	}

	$is_event = $product_object && 'event' === $product_object->get_type() ? true : false;
	?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			// price tab
			jQuery('#general_product_data .pricing').addClass('show_if_event');

			<?php
				if($is_event){ ?>
					jQuery('#general_product_data .pricing').show();

			<?php	}
			?>
		});
	</script>
	<?php
}
add_action('admin_footer', 'event_product_js');

// save data 
function save_event_data($post_id) {
	$price = isset( $_POST['_event_price'] ) ? sanitize_text_field( $_POST['_event_price'] ) : '';
	update_post_meta($post_id, '__event_price', $price);
}
add_action('woocommerce_process_product_meta_event', 'save_event_data');

function add_cart_button_event() {
	global $product;
	$id = $product->get_id();
	if( !empty(get_field('memberships_id', 'option')) ) :
		$membersips = get_field('memberships_id', 'option');
		$array_membersips = explode(',', $membersips);
	endif;

	if(is_user_logged_in() && WC_Product_Factory::get_product_type($id) == 'event' && pmpro_hasMembershipLevel($array_membersips)) {
		echo do_shortcode( '[membership]
		<form class="cart" action="" method="post" enctype="multipart/form-data">
				<div class="quantity">
					<label class="screen-reader-text" for="quantity_602b00c68ae64">Testaa quantity</label>
			<input type="number" id="quantity_602b00c68ae64" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric">
				</div>
				<div id="wc-stripe-payment-request-wrapper" style="clear:both;padding-top:1.5em;display:none;">
				<div id="wc-stripe-payment-request-button">
									<!-- A Stripe Element will be inserted here. -->
				</div>
			</div>
					<p id="wc-stripe-payment-request-button-separator" style="margin-top:1.5em;text-align:center;display:none;">— OR —</p>
			
			<button type="submit" name="add-to-cart" value="' . $id . '" class="single_add_to_cart_button button alt">Add to cart</button>

				</form>
		[/membership]' );
	}else {
		_e('<p>This content is for members only.</p>', 'coursector');
	}
}
add_action('woocommerce_single_product_summary', 'add_cart_button_event');

function event_post_type() {

	/**
	 * Post Type: events.
	 */

	$labels = [
		"name" => __( "events", "coursector" ),
		"singular_name" => __( "event", "coursector" ),
		"menu_name" => __( "My events", "coursector" ),
		"all_items" => __( "All events", "coursector" ),
		"add_new" => __( "Add new", "coursector" ),
		"add_new_item" => __( "Add new event", "coursector" ),
		"edit_item" => __( "Edit event", "coursector" ),
		"new_item" => __( "New event", "coursector" ),
		"view_item" => __( "View event", "coursector" ),
		"view_items" => __( "View events", "coursector" ),
		"search_items" => __( "Search events", "coursector" ),
		"not_found" => __( "No events found", "coursector" ),
		"not_found_in_trash" => __( "No events found in trash", "coursector" ),
		"parent" => __( "Parent event:", "coursector" ),
		"featured_image" => __( "Featured image for this event", "coursector" ),
		"set_featured_image" => __( "Set featured image for this event", "coursector" ),
		"remove_featured_image" => __( "Remove featured image for this event", "coursector" ),
		"use_featured_image" => __( "Use as featured image for this event", "coursector" ),
		"archives" => __( "event archives", "coursector" ),
		"insert_into_item" => __( "Insert into event", "coursector" ),
		"uploaded_to_this_item" => __( "Upload to this event", "coursector" ),
		"filter_items_list" => __( "Filter events list", "coursector" ),
		"items_list_navigation" => __( "events list navigation", "coursector" ),
		"items_list" => __( "events list", "coursector" ),
		"attributes" => __( "events attributes", "coursector" ),
		"name_admin_bar" => __( "event", "coursector" ),
		"item_published" => __( "event published", "coursector" ),
		"item_published_privately" => __( "event published privately.", "coursector" ),
		"item_reverted_to_draft" => __( "event reverted to draft.", "coursector" ),
		"item_scheduled" => __( "event scheduled", "coursector" ),
		"item_updated" => __( "event updated.", "coursector" ),
		"parent_item_colon" => __( "Parent event:", "coursector" ),
	];

	$args = [
		"label" => __( "events", "coursector" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "event", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "event", $args );
}

add_action( 'init', 'event_post_type' );

function related_events() {
	ob_start();
	$related_events = get_field('releated_events');
	if( !empty(get_field('memberships_id', 'option')) ) :
		$membersips = get_field('memberships_id', 'option');
		$array_membersips = explode(',', $membersips);
	endif;

	if($related_events && pmpro_hasMembershipLevel($array_membersips)) : 
	?>
	<h4><?php _e("Related events", 'coursector' ); ?></h4>
		<ul style="list-style: none; margin-left: 0;">
	<?php
		foreach( $related_events as $related_event ) :
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $related_event->ID ));
	?>
		<li style="max-width: 33%;">
			<img src="<?= $image[0]; ?>">
			<?php echo do_shortcode( '[add_to_cart show_price="false" style="" id="' . $related_event->ID . '"]' ); ?>
		</li>
	<?php
		endforeach; ?>
			</ul>
		<?php
	endif;
	return ob_get_clean();
}
add_shortcode( 'relatedevents', 'related_events' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Additional Settings',
		'menu_title'	=> 'Additional Settings',
		'menu_slug' 	=> 'additional-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

};
